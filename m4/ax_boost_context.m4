# ===========================================================================
#       http://www.gnu.org/software/autoconf-archive/ax_boost_context.html
# ===========================================================================
#
# SYNOPSIS
#
#   AX_BOOST_CONTEXT
#
# DESCRIPTION
#
#   Test for Context library from the Boost C++ libraries. The macro requires a
#   preceding call to AX_BOOST_BASE. Further documentation is available at
#   <http://randspringer.de/boost/index.html>.
#
#   This macro calls:
#
#     AC_SUBST(BOOST_CONTEXT_LIB)
#
#   And sets:
#
#     HAVE_BOOST_CONTEXT
#
# LICENSE
#
#   Copyright (c) 2008 Thomas Porschberg <thomas@randspringer.de>
#   Copyright (c) 2008 Pete Greenwell <pete@mu.org>
#
#   Copying and distribution of this file, with or without modification, are
#   permitted in any medium without royalty provided the copyright notice
#   and this notice are preserved. This file is offered as-is, without any
#   warranty.


AC_DEFUN([AX_BOOST_CONTEXT],
[
	AC_ARG_WITH([boost-context],
	AS_HELP_STRING([--with-boost-context@<:@=special-lib@:>@],
                   [use the CONTEXT library from boost - it is possible to specify a certain library for the linker
                        e.g. --with-boost-context=boost_context-gcc-mt ]),
        [
        if test "$withval" = "no"; then
			want_boost="no"
        elif test "$withval" = "yes"; then
            want_boost="yes"
            ax_boost_user_context_lib=""
        else
		    want_boost="yes"
		ax_boost_user_context_lib="$withval"
		fi
        ],
        [want_boost="yes"]
	)

	if test "x$want_boost" = "xyes"; then
        AC_REQUIRE([AC_PROG_CC])
		CPPFLAGS_SAVED="$CPPFLAGS"
		CPPFLAGS="$CPPFLAGS $BOOST_CPPFLAGS"
		export CPPFLAGS

		LDFLAGS_SAVED="$LDFLAGS"
		LDFLAGS="$LDFLAGS $BOOST_LDFLAGS"
		export LDFLAGS

        AC_CACHE_CHECK(whether the Boost::Context library is available,
			ax_cv_boost_context,
		[AC_LANG_PUSH([C++])
			 AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[ @%:@include <boost/context/all.hpp> ]],
			[[
			boost::context::guarded_stack_allocator alloc;
			void * sp( alloc.allocate(8*1024*1024));
			std::size_t size( boost::context::guarded_stack_allocator::minimum_stacksize());
			boost::context::fcontext_t  * fc( boost::context::make_fcontext( sp, size, NULL));
			boost::context::jump_fcontext( fc, fc, 0);
			return 0;
			]])], 
				ax_cv_boost_context=yes, ax_cv_boost_context=no)
				AC_LANG_POP([C++])
		])
		if test "x$ax_cv_boost_context" = "xyes"; then

			AC_DEFINE(HAVE_BOOST_CONTEXT,,[define if the Boost::Context library is available])
            BOOSTLIBDIR=`echo $BOOST_LDFLAGS | sed -e 's/@<:@^\/@:>@*//'`

			LDFLAGS_SAVE=$LDFLAGS
            if test "x$ax_boost_user_context_lib" = "x"; then
                for libextension in `ls -r $BOOSTLIBDIR/libboost_context* 2>/dev/null | sed 's,.*/lib,,' | sed 's,\..*,,'` ; do
                     ax_lib=${libextension}
				    AC_CHECK_LIB($ax_lib, [jump_fcontext],
                                 [BOOST_CONTEXT_LIB="-l$ax_lib"; AC_SUBST(BOOST_CONTEXT_LIB) link_context="yes"; break],
                                 [link_context="no"])
				done
                if test "x$link_context" != "xyes"; then
                for libextension in `ls -r $BOOSTLIBDIR/boost_context* 2>/dev/null | sed 's,.*/,,' | sed -e 's,\..*,,'` ; do
                     ax_lib=${libextension}
				    AC_CHECK_LIB($ax_lib, [jump_fcontext],
                                 [BOOST_CONTEXT_LIB="-l$ax_lib"; AC_SUBST(BOOST_CONTEXT_LIB) link_context="yes"; break],
                                 [link_context="no"])
				done
                fi

            else
               for ax_lib in $ax_boost_user_context_lib boost_context-$ax_boost_user_context_lib; do
				      AC_CHECK_LIB($ax_lib, [jump_fcontext],
                                   [BOOST_CONTEXT_LIB="-l$ax_lib"; AC_SUBST(BOOST_CONTEXT_LIB) link_context="yes"; break],
                                   [link_context="no"])
                  done

            fi
            if test "x$ax_lib" = "x"; then
                AC_MSG_ERROR(Could not find a version of the library!)
            fi
			if test "x$link_context" = "xno"; then
				AC_MSG_ERROR(Could not link against $ax_lib !)
			fi
		fi

		CPPFLAGS="$CPPFLAGS_SAVED"
	LDFLAGS="$LDFLAGS_SAVED"
	fi
])

