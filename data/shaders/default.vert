// Input
uniform mat4 transform;
uniform mat4 projection;

attribute vec2 position;
attribute vec2 texcoord;

varying vec2 fig_texcoord;

void main()
{
  gl_Position = projection * (transform * vec4(position, 0.0, 1.0));

  fig_texcoord = texcoord;
}
