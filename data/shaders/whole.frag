uniform sampler2D figure;
uniform float alpha;

varying vec2 fig_texcoord;

void main()
{
	gl_FragColor = texture2D(figure, fig_texcoord) * alpha;
}
