uniform sampler2D figure;
uniform sampler2D mask;
uniform float alpha;
uniform int mask_val;

varying vec2 fig_texcoord;

void main()
{
	if(abs(texture2D(mask, fig_texcoord).x * 255.0 - float(mask_val)) > 0.5) {
		discard;

/*
		// Debug drawing of the rectangle:
		gl_FragColor = vec4(0.0, 0.0, 0.9, 0.5 * alpha);
		return;
*/
	}

	gl_FragColor = texture2D(figure, fig_texcoord);
	gl_FragColor.a = gl_FragColor.a * alpha;
}
