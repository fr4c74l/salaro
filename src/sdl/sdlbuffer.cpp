#include <algorithm>
#include <cassert>
#include "sdlbuffer.hpp"
#include "plainbuffer.hpp"

using namespace std;

unique_ptr<ImgBuffer> SDLBuffer::createFromSurface(SDL_Surface* sur)
{
	bool must_convert;
	bool surely_greyscale = false;
	SDL_Palette* palette;

	// Little-endian only code:
	if((sur->format->BytesPerPixel == 4
			&& sur->format->Rmask == 0x000000ff
			&& sur->format->Gmask == 0x0000ff00
			&& sur->format->Bmask == 0x00ff0000
			&& sur->format->Amask == 0xff000000)
		|| (sur->format->BytesPerPixel == 3
				&& sur->format->Rmask == 0x0000ff
				&& sur->format->Gmask == 0x00ff00
				&& sur->format->Bmask == 0xff0000))
		must_convert = false;
	else if(sur->format->BytesPerPixel == 1)
	{
		assert(sur->format->palette);
		palette = sur->format->palette;

		int i;
		if(palette->ncolors == 256)
			for(i = 0; i < 256; ++i) {
				if(palette->colors[i].r != i || palette->colors[i].g != i || palette->colors[i].b != i) {
					break;
				}
			}

		// Must convert if not canonical greyscale
		must_convert = (i < 256);

		if(must_convert) {
			int j;
			// Must convert, but is it still greyscale?
			for(j = 0; j < palette->ncolors; ++j) {
				if(palette->colors[j].r != palette->colors[j].g
					|| palette->colors[j].g != palette->colors[j].b) {
					break; // It is colored!
				}
			}
			surely_greyscale = (j == palette->ncolors);
		}
	}
	else
		must_convert = true;


	// If image is not ready for usage as texture (or not byte aligned),
	// copy to a new buffer, converting
	if(must_convert)
	{
		uint8_t ibpp = sur->format->BytesPerPixel;
		uint8_t obpp = surely_greyscale ? 1 : (ibpp == 1 || !sur->format->Amask ? 3 : 4);
		unique_ptr<ImgBuffer> ret(new PlainBuffer(sur->w, sur->h, obpp));
		uint8_t* in_buff = (uint8_t*)sur->pixels;
		uint8_t* out_buff = ret->getData();
		for(int y = 0; y < sur->h; ++y) {
			uint16_t oy = (sur->h -y -1);
			uint16_t opitch = ret->getPitch();

			// Little-endian only code (I think), also may make unaligned memory access:
			for(int x = 0; x < sur->w; ++x) {
				uint32_t packed = *(uint32_t*) &in_buff[y * sur->pitch + x * ibpp];
				Uint8 *out = &out_buff[oy * opitch + x * obpp];
				switch(obpp) {
				case 1:
					*out = palette->colors[packed & 255].r;
					break;
				case 3:
					SDL_GetRGB(packed, sur->format, out, out + 1, out + 2);
					break;
				case 4:
					SDL_GetRGBA(packed, sur->format, out, out + 1, out + 2, out + 3);
					break;
				}
			}
		}

		SDL_FreeSurface(sur);

		return ret;
	}
	else
	{
		return unique_ptr<ImgBuffer>(new SDLBuffer(sur));
	}
}

SDLBuffer::SDLBuffer(SDL_Surface* s)
{
	assert(SDL_MUSTLOCK(s) == 0);
	surface = s;
}

SDLBuffer::~SDLBuffer() {
	SDL_FreeSurface(surface);
}

uint8_t* SDLBuffer::getData() const {
	return (uint8_t*)surface->pixels;
}

uint8_t SDLBuffer::getBPP() const {
	return surface->format->BytesPerPixel;
}

uint16_t SDLBuffer::getWidth() const {
	return surface->w;
}

uint16_t SDLBuffer::getHeight() const {
	return surface->h;
}

uint16_t SDLBuffer::getPitch() const {
	return surface->pitch;
}
