#include <iostream>
#include <cstdlib>
#include <memory>

#include <GL/glew.h>
#ifdef WIN32
#include <windows.h>
#include <GL/wglew.h>
#else
#include <GL/glxew.h>
#endif

#include <SDL.h>
#include <SDL_image.h>

#include "input.hpp"
#include "game.hpp"
#include "options.hpp"
#include "sdlbuffer.hpp"
#include "config.hpp"

#include "native.hpp"

using namespace std;

static bool v_sync_enabled = true;
static bool draw_needed = true;

void list_video_modes(vector<string> &out)
{
	SDL_Rect** modes = SDL_ListModes(NULL, SDL_FULLSCREEN | SDL_HWSURFACE);
	while(*modes) {
		char str[15];
		snprintf(str, 15, "%dx%d", (*modes)->w, (*modes)->h);
		out.push_back(string(str));
		++modes;
	}
}

void post_redraw()
{
	draw_needed = true;
}

unique_ptr<ImgBuffer> new_image_from_file(const std::string& filename)
{
	SDL_Surface* sur = IMG_Load((std::string(DATA_DIR)+filename).c_str());

	if(!sur)
		return unique_ptr<ImgBuffer>();
	else
		return SDLBuffer::createFromSurface(sur);
}

vector<char> read_file(const std::string& filename)
{
	ifstream file(filename);

	file.seekg(0, std::ios::end);
	vector<char> ret(file.tellg());
	file.seekg(0, std::ios::beg);

	file.read(&ret[0], ret.size());

	return ret;
}

static void initialize_SDL()
{
	/* SDL Startup */
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
	{
		cerr << "Unable to initialize SDL: " << SDL_GetError() << endl;
		exit(1);
	}
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	{
		int video_flags = SDL_OPENGL | SDL_HWSURFACE | SDL_HWACCEL;
		/*if(Options::fullscreen)
			video_flags |= SDL_FULLSCREEN;*/
		SDL_SetVideoMode(Options::width, Options::height, 0, video_flags);
	}
	SDL_WM_SetCaption("Salaro", NULL);
	SDL_ShowCursor(SDL_ENABLE);
	SDL_EnableUNICODE(1);

	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
}

static void initialize_gl_context()
{
	// Using GLEW to get the OpenGL functions
	GLenum err = glewInit();
	if(err != GLEW_OK) {
		cerr << "Unable to initialize GLEW:\n"
		<< glewGetErrorString(err) << endl;
		exit(1);
	}

	if(! GLEW_VERSION_2_1)
	{
		const char *msg = "Salaro requires at least OpenGL 2.1";
		#ifdef WIN32
		MessageBoxA(NULL, msg, NULL, MB_OK);
		#else
		cerr << msg << endl;
		#endif
		exit(1);
	}

	// Enable V-Sync
	#ifdef WIN32
	if(WGLEW_EXT_swap_control)
		wglSwapIntervalEXT(1);
	#else
	if(GLXEW_SGI_swap_control)
		glXSwapIntervalSGI(1);
	#endif
	else
		v_sync_enabled = false;
}

static void main_loop()
{
	const int FPS = 60;
	uint64_t frame_count = 0;
	bool running = true;
	Uint32 start;
	Uint32 ticks;

	start = ticks = SDL_GetTicks();
	while(running) 
	{
		running = Input::handle();
		bool update_needed = Game::update();

		if(draw_needed) {
			Game::draw();
			SDL_GL_SwapBuffers();
			draw_needed = false;
		}

		if(update_needed) {
			// Runnig animation or something
			if(!v_sync_enabled) {
				// Fix framerate:
				// TODO: maybe clk_div is useful here...
				const int period = 1000 / FPS;
				Uint32 now = SDL_GetTicks();
				int delay = period - int(now - ticks);
				if(delay > 0)
					SDL_Delay(delay);
				ticks = now;
			}
		} else {
			// Just waiting for user input
			Input::wait_event();
			if(!v_sync_enabled) {
				ticks = SDL_GetTicks();
			}
		}
		++frame_count;
	}
	cout << frame_count << " frames rendered at " << frame_count / ((SDL_GetTicks() - start) / 1000.0) << " FPS.\n";
}

int main(int argc, char **argv)
{
	if (Options::parse_args(argc, argv))
		return 1;

	initialize_SDL();
	initialize_gl_context();
	Game::initialize();

	main_loop();
}

