#pragma once

#include <memory>
#include <SDL.h>
#include "imgbuffer.hpp"

class SDLBuffer: public ImgBuffer {
public:
	static std::unique_ptr<ImgBuffer> createFromSurface(SDL_Surface *surface);

	virtual ~SDLBuffer();

	virtual uint8_t* getData() const;

	virtual uint8_t getBPP() const;
	virtual uint16_t getWidth() const;
	virtual uint16_t getHeight() const;
	virtual uint16_t getPitch() const;

private:
	SDLBuffer(SDL_Surface *surface);
	SDL_Surface *surface;
};
