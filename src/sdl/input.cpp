#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>
#include <string>
#include <assert.h>
#include <SDL.h>
#include "game.hpp"
#include "native.hpp"
#include "options.hpp"
#include "mouseinput.hpp"

#include "input.hpp"

namespace Input
{

static MouseInput processor;

static float x_transform(uint16_t x)
{
	return 2.0f * float(x) / float(Options::width) - 1.0f;
}

static float y_transform(uint16_t y)
{
	static float h = 2.0f * Options::half_h;
	return h * float(Options::height - y) / float(Options::height) - Options::half_h;
}

static void
handle_click(const SDL_MouseButtonEvent& e)
{
	if(e.state == SDL_PRESSED) {
		void (MouseInput::*func)(float x, float y, bool param) = nullptr;
		bool param;

		switch(e.button) {
		case SDL_BUTTON_LEFT:
			func = &MouseInput::hold;
			param = false;
			break;
		case SDL_BUTTON_RIGHT:
			func = &MouseInput::hold;
			param = true;
			break;
		case SDL_BUTTON_WHEELDOWN:
			func = &MouseInput::turn;
			param = false;
			break;
		case SDL_BUTTON_WHEELUP:
			func = &MouseInput::turn;
			param = true;
			break;
		}

		if(func) {
			(processor.*func)(x_transform(e.x), y_transform(e.y), param);
		}
	} else if(e.button == SDL_BUTTON_LEFT || e.button == SDL_BUTTON_RIGHT) {
		processor.release();
	}
}

bool
handle()
{
	bool run = true;
	SDL_Event e;

	while(SDL_PollEvent(&e))
	{
		switch(e.type)
		{
		case SDL_QUIT:
			run = false;
			break;
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			if(processor.get_target())
				handle_click(e.button);
			break;
		case SDL_MOUSEMOTION:
			if(processor.get_target() && e.motion.state & (SDL_BUTTON(1) | SDL_BUTTON(3)))
				processor.drag(x_transform(e.motion.x), y_transform(e.motion.y));
			break;
		case SDL_VIDEOEXPOSE:
			post_redraw();
			break;
		}
	}

	return run;
}

void
wait_event()
{
	SDL_WaitEvent(NULL);
}

}

void input_set_puzzle(Puzzle* puzzle)
{
	Input::processor.set_target(puzzle);
}


