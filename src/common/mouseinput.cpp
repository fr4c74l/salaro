#include <cmath>
#include "mouseinput.hpp"

void MouseInput::hold(float x, float y, bool to_rot)
{
	auto c = target->pick(x, y);
	if(c) {
		// Get hold of piece
		selected = Hold(c);

		// Choose kind of hold
		selection_is_rot = to_rot;
		if(to_rot) {
			Vector2 delta = Vector2(x, y) - c->getPos();
			last_rot_drag_angle = atan2f(delta.y, delta.x);
		}

		// Clear cached piece for rotation
		last_rot.reset();

		hold_point = Vector2(x, y);
		post_redraw();
	}
}

void MouseInput::drag(float x, float y)
{
	if(auto sel = selected.lock()) {
		if(selection_is_rot) {
			Vector2 delta = Vector2(x, y) - sel->getPos();
			float angle = atan2f(delta.y, delta.x);
			sel->rotate(sel->getPos(), angle - last_rot_drag_angle);
			last_rot_drag_angle = angle;
		} else {
			sel->move(x - hold_point.x, y - hold_point.y);
			hold_point = Vector2(x, y);
		}
	}
}

void MouseInput::release()
{
	if(!selected.expired())
	{
		{
			auto sel = selected.lock();
			target->tryMerge(sel.get());
		}
		selected.reset();
	}
}

void MouseInput::turn(float x, float y, bool to_right)
{
	Vector2 pos(x, y);
	if(last_rot.expired() || last_rot_pos != pos) {
		last_rot_pos = pos;

		if(!selected.expired()) {
			if(selection_is_rot)
				return;
			last_rot = selected.get_ref();
		} else {
			last_rot = target->pick(x, y);
		}
	}

	if(auto lr = last_rot.lock())
		lr->rotate(pos, to_right ? 0.05f : -0.05f);
}

void MouseInput::reset()
{
	selected.reset();
	last_rot.reset();
}
