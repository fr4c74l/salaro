#include <cassert>
#include "puzzletransition.hpp"
#include "interlude.hpp"
#include "math.hpp"
#include "native.hpp"

PuzzleTransition::PuzzleTransition(Puzzle& prev, std::unique_ptr<Puzzle>&& next,
				   const Vector2& offset, float s):
prev_fig(std::move(prev.figure)),
next_puzzle(std::move(next)),
target_pos(offset),
target_scale(s),
frame_total((unsigned int)(60.0f / s)),
frame_count(0u)
{
	assert(prev.parts.size() == 1);
	Cluster& sole = **prev.parts.begin();
	orig_pos = sole.getPos();
	orig_rot = normalize_rot(sole.getRot());

	float inv_scale = 1.0f / s;
	next_displace = translation(-target_pos * inv_scale) * scale(inv_scale);
}

void PuzzleTransition::setup_draw()
{
	Interlude::setup_draw();
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
}

bool PuzzleTransition::update()
{
	float t = (-cosf(frame_count * M_PI / float(frame_total)) + 1.0f) * 0.5f;
	float ct = 1.0f - t;
	next_alpha = t*t*t*t*t; // t⁵, much faster than pow(t, 5)

	prev_transform = translation(orig_pos * ct + target_pos * t)
	* rotation(orig_rot * ct)
	* scale(ct * (1.0f - target_scale) + target_scale);

	next_transform = prev_transform * next_displace;

	post_redraw();
	++frame_count;
	return true;
}

void PuzzleTransition::draw()
{
	Interlude::shader.setTransform(prev_transform);
	prev_fig.enable();
	Interlude::alpha.set(1.0f);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	Interlude::shader.setTransform(next_transform);
	next_puzzle->figure.enable();
	Interlude::alpha.set(next_alpha);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

