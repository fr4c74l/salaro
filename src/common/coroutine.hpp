#pragma once
#include <cassert>
#include <functional>
#include <boost/context/all.hpp>

/** Coroutine implementation based on Boost.Context.
 */
class CoRoutine
{
public:
	typedef std::function<void()> YieldFunc;

	template<class Callable>
	explicit CoRoutine(Callable &&c)
	{
		RunParameters<Callable> params;
		params.c = &c;
		params.obj = this;

		stack_allocate();
		coroutine = boost::context::make_fcontext(coroutine_stack,
							  STACK_SIZE, &run_it<Callable>);

		// Pass the parameters so they can be copied.
		boost::context::jump_fcontext(&main, coroutine,
					      reinterpret_cast<intptr_t>(&params),
					      true);
	}

	~CoRoutine() {
		if(coroutine_stack) {
			// Pass 1, so Terminate will be thrown and the coroutine will (hopefully) end.
			intptr_t ret = boost::context::jump_fcontext(&main, coroutine, 1, true);
			assert(ret && "Terminate exception was swallowed by coroutine!");
			stack_deallocate();
		}
	}

	void call()
	{
		if(boost::context::jump_fcontext(&main, coroutine, 0, true)) {
			// Has ended.
			stack_deallocate();
		}
	}

	bool done() {
		return !coroutine_stack;
	}

private:
	// If the CoRoutine object is destroyed before finishing,
	// this class will be thrown by "yeld()"
	class Terminate {};

	/** Must only be called by inside the coroutine. */
	void yield()
	{
		if(boost::context::jump_fcontext(coroutine, &main, 0, true)) {
			// Termination requested.
			throw Terminate();
		}
	}

	template<class Callable>
	struct RunParameters {
		Callable* c;
		CoRoutine* obj;
	};

	template<class Callable>
	static void run_it(intptr_t param)
	{
		// Non-trivial variables must not be declared in this outer scope,
		// because we will never return from it, thus the destructors
		// will never be called.

		// This is a plain pointer, so it can be here:
		CoRoutine* obj = reinterpret_cast<RunParameters<Callable>*>(param)->obj;

		try {
			Callable c = std::move(*reinterpret_cast<RunParameters<Callable>*>(param)->c);

			// "param" pointer will only be valid until the following...
			obj->yield();

			c([=](){ obj->yield(); });
		} catch (Terminate t) {
			// Forced termination.
		}

		// Pass 1 to inform the end of the procedure.
		boost::context::jump_fcontext(obj->coroutine, &obj->main, 1, true);
	}

	void stack_allocate();
	void stack_deallocate();

	static const size_t STACK_SIZE;
	void *coroutine_stack;

	boost::context::fcontext_t main;
	boost::context::fcontext_t* coroutine;
};
