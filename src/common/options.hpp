#pragma once

#include <string>


namespace Options
{

// Video options
extern bool fullscreen;
extern int width;
extern int height;
extern float half_h;

void set_dimensions(int w, int h);
int parse_args(int argc,char** argv);
int load_config(std::string configfile);

}

