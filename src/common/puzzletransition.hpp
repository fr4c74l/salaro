#pragma once

#include <string>
#include <memory>
#include "puzzle.hpp"
#include "puzzleintro.hpp"

class PuzzleTransition: public Context
{
public:
	PuzzleTransition(Puzzle& prev, std::unique_ptr<Puzzle>&& next,
			 const Vector2& offset, float scale);

	void setup_draw();

	bool update();
	void draw();

	bool done()
	{
		return frame_count >= frame_total;
	}

	std::unique_ptr<Puzzle> getPuzzle() {
		return std::move(next_puzzle);
	}

private:
	Texture prev_fig;
	std::unique_ptr<Puzzle> next_puzzle;

	Vector2 target_pos;
	float target_scale;

	Vector2 orig_pos;
	float orig_rot;

	Matrix4 prev_transform;

	Matrix4 next_transform;
	Matrix4 next_displace;
	float next_alpha;

	unsigned int frame_total;
	unsigned int frame_count;
};
