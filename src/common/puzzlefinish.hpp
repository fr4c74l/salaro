#pragma once
#include "puzzle.hpp"

class PuzzleFinish: public Context
{
public:
	PuzzleFinish(Puzzle& prev);

	void setup_draw();

	bool update();
	void draw();

	bool done()
	{
		return frame_count >= FRAME_TOTAL;
	}

private:
	Texture figure;

	Vector2 orig_pos;
	float orig_rot;

	Matrix4 transformation;

	float target_scale;

	static const unsigned int FRAME_TOTAL;
	unsigned int frame_count;
};
