#include "texture.hpp"

void Texture::setupFromBuffer(const ImgBuffer& buf, GLint filter)
{
	bool fresh = !tex;
	if(fresh)
		glGenTextures(1, &tex);

	int mode;
	switch(buf.getBPP()) {
		case 1:
			mode = GL_LUMINANCE;
			break;
			// case 2 could be GL_LUMINANCE_ALPHA? There are no such image type.
		case 3:
			mode = GL_RGB;
			break;
		case 4:
			mode = GL_RGBA;
			break;
		default:
			assert(0 && "Bizarre number of components in loaded image!");
			break;
	}

	glBindTexture(GL_TEXTURE_2D, tex);

	glPixelStorei(GL_UNPACK_ALIGNMENT, buf.getAlignment());
	glTexImage2D(GL_TEXTURE_2D, 0, mode, buf.getWidth(), buf.getHeight(), 0, mode, GL_UNSIGNED_BYTE, buf.getData());

	if(fresh) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
	}
}

void Texture::enable(GLenum level) const
{
	glActiveTexture(level);
	glBindTexture(GL_TEXTURE_2D, tex);
}
