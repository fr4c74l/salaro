#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <getopt.h>

#include "options.hpp"

using namespace std;

namespace Options
{

// Video options
bool fullscreen = false;
int width = 800;
int height = 600;
float half_h; // In game coordinates

void set_dimensions(int w, int h)
{
	width = w;
	height = h;
	half_h = float(height) / float(width);
}

// TODO: this should be specific to SDL version
void usage()
{
	puts(
			"\n"
			"Usage: salaro [options]\n\n"
			"Video options:\n"
			"\t-w/--width [number] \t: set window's width\n"
			"\t-h/--height [number] \t: set window's height\n"
			"\t-f/--fullscreen\t\t: be fullscreen\n"
			"Salaro is free software.  See COPYING for details.\n"
	);
}

int parse_args(int argc, char** argv)
{
	static struct option long_options[] =
	{
			{"full-screen",	no_argument,				0,	'f'},
			{"width",				required_argument,	0,	'w'},
			{"height",			required_argument,	0,	'h'},
			{0, 0, 0, 0}
	};

	char option;
	int option_index = 0;

	while ((option = getopt_long(argc, argv, "fw:h:", long_options, &option_index)) != EOF)
	{
		switch (option)
		{
		case 'f':
			fullscreen = true;
			break;
		case 'w':
			width = atoi(optarg);
			break;
		case 'h':
			height = atoi(optarg);
			break;
		case '?':
			usage();
			return 1;
		}
	}

	half_h = float(height) / float(width);

	return 0;
}

}

