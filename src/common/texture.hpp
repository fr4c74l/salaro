#pragma once

#include "gl.hpp"
#include "imgbuffer.hpp"

class Texture
{
public:
	Texture():
	tex(0)
	{}

	Texture(const ImgBuffer& buf):
	tex(0)
	{
		setupFromBuffer(buf);
	}
	
	Texture(const Texture& o) = delete;
	Texture(Texture&& o) {
		*this = std::move(o);
	}

	~Texture()
	{
		freeTex();
	}

	Texture& operator=(const Texture& o) = delete;
	Texture& operator=(Texture&& o) {
		freeTex();
		tex = o.tex;
		o.tex = 0;
		return *this;
	}

	void setupFromBuffer(const ImgBuffer& buf, GLint filter=GL_LINEAR);
	void enable(GLenum level = GL_TEXTURE0) const;

private:
	void freeTex()
	{
		if(tex)
			glDeleteTextures(1, &tex);
	}

	GLuint tex;
};