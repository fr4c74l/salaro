#pragma once

#include "puzzle.hpp"

class InputHandler
{
public:
	virtual ~InputHandler()
	{}

	void set_target(Puzzle* tgt)
	{
		reset();
		target = tgt;
	}

	Puzzle* get_target()
	{
		return target;
	}

protected:
	virtual void reset() = 0;

	Puzzle* target;
};