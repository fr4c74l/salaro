#include "vector2.hpp"
#include "options.hpp"
#include "math.hpp"
#include "vertex.hpp"

#include "interlude.hpp"

namespace Interlude
{

CamShader shader;
GLuint vbo;
Uniform alpha;

void initialize() {
	const char *sources[] = {"whole.frag", "default.vert", NULL};
	shader.setup_shader(sources);
	shader.enable();
	shader.getUniform("figure").set(0);
	alpha = shader.getUniform("alpha");

	shader.setProjection(ortho(-1, 1, -Options::half_h, Options::half_h, -1, 1));

	glGenBuffers(1, &vbo);

	Vertex vert[4];

	float tex_bottom = 1.0f;
	float tex_top = 0.0f;
	float tex_left = 0.0f;
	float tex_right = 1.0f;

	vert[0].tex = Vector2(tex_left, tex_bottom);
	vert[0].pos = Vector2(-0.75f, -0.5f);

	vert[1].tex = Vector2(tex_right, tex_bottom);
	vert[1].pos = Vector2(0.75f, -0.5f);

	vert[2].tex = Vector2(tex_right, tex_top);
	vert[2].pos = Vector2(0.75f, 0.5f);

	vert[3].tex = Vector2(tex_left, tex_top);
	vert[3].pos = Vector2(-0.75f, 0.5f);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vert), vert, GL_STATIC_DRAW);
}

void setup_draw()
{
	shader.enable();

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)sizeof(Vector2));
}

}