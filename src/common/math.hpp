#pragma once

#include <cmath>

#include "matrix4.hpp"
#include "vector2.hpp"

// Creates orthographic projection matrix
Matrix4
ortho(float left, float right, float bottom, float top, float near, float far);

// Creates a rotation matrix
Matrix4 rotation(float angle);

// Creates a translation matrix.
Matrix4 translation(Vector2 delta);

// Creates a scale matrix.
Matrix4 scale(Vector2 size);
Matrix4 scale(float size);

Vector2 operator/(float s, const Vector2& v);

// From a rotation, in radians, returns an angle between -pi and pi
float normalize_rot(float r);
