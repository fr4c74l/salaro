#pragma once

#include <inttypes.h>
#include <memory>
#include "matrix4.hpp"
#include "animator.hpp"

namespace Game
{
	/** Process and renders a game frame. */
	bool update();
	void draw();
	void initialize();

	/** Register animation. */
	void add_animation(std::shared_ptr<Animator> anim);
}
