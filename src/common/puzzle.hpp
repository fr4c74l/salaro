#pragma once

#include <inttypes.h>
#include <list>
#include <memory>
#include "config.hpp"
#include "context.hpp"
#include "piece.hpp"
#include "cluster.hpp"
#include "gl.hpp"
#include "texture.hpp"

class PuzzleIntro;
class PuzzleTransition;
class PuzzleFinish;

class Puzzle: public Context
{
public:
	static void initialize();

	template<class Splitter>
	Puzzle(const ImgBuffer& figure_img, Splitter&& splitter):
	mask_buf(splitter(parts)),
	figure(figure_img),
	finished(false)
	{
		split.setupFromBuffer(*mask_buf, GL_NEAREST);
	}

	virtual ~Puzzle()
	{}

	void setup_draw();

	virtual bool update();
	void draw();

	/* Has user completed the puzzle? */
	bool done();

	/* Input access functions. */
	std::shared_ptr<Cluster> pick(float x, float y);
	bool tryMerge(Cluster* c);

private:
	/// Responsible for pre/post animations:
	friend class PuzzleIntro;
	friend class PuzzleTransition;
	friend class PuzzleFinish;

	std::list<std::shared_ptr<Cluster> > parts;
	std::unique_ptr<ImgBuffer> mask_buf;

	Texture figure;
	Texture split;

	bool finished;
};
