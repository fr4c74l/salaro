#pragma once

#include "native.hpp"

/** Grants holding of a puzzle cluster to owner;
 * used by input to rotate/manipulate that piece. */
class Hold
{
public:
	Hold(Hold const&) = delete;
	Hold& operator=(Hold const&) = delete;

	Hold() = default;

	Hold(std::weak_ptr<Cluster> c):
	cluster(std::move(c))
	{
		if(auto ptr = cluster.lock()) {
			ptr->setAlpha(0.7f);
			post_redraw();
		}
	}

	Hold(Hold&& other)
	{
		*this = std::move(other);
	}

	Hold& operator=(Hold&& other)
	{
		reset();
		cluster = other.cluster;
		other.cluster.reset();
		return *this;
	}

	~Hold()
	{
		reset();
	}

	void reset()
	{
		if(auto ptr = cluster.lock()) {
			ptr->setAlpha(1.0f);
			post_redraw();
			cluster.reset();
		}
	}

	bool expired() const
	{
		return cluster.expired();
	}

	std::shared_ptr<Cluster> lock() const
	{
		return cluster.lock();
	}

	const std::weak_ptr<Cluster>& get_ref() const
	{
		return cluster;
	}

private:
	std::weak_ptr<Cluster> cluster;
};
