#pragma once

#include "vector2.hpp"

struct Vertex {
	Vector2 pos;
	Vector2 tex;
};
