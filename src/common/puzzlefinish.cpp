#include "interlude.hpp"
#include "math.hpp"
#include "options.hpp"
#include "native.hpp"

#include "puzzlefinish.hpp"

const unsigned int PuzzleFinish::FRAME_TOTAL = 150;

PuzzleFinish::PuzzleFinish(Puzzle& prev):
figure(std::move(prev.figure))
{
	float fill_h = Options::half_h * 2.0f;
	target_scale = std::min(4.0f / 3.0f, fill_h) * 0.99f;

	assert(prev.parts.size() == 1);
	Cluster& sole = **prev.parts.begin();
	orig_pos = sole.getPos();
	orig_rot = normalize_rot(sole.getRot());
}

void PuzzleFinish::setup_draw()
{
	Interlude::setup_draw();
	Interlude::alpha.set(1.0f);
	figure.enable();
}

bool PuzzleFinish::update()
{
	if(done())
		return false;

	float t = (-cosf(frame_count * M_PI / float(FRAME_TOTAL)) + 1.0f) * 0.5f;
	float ct = 1.0f - t;

	transformation = translation(orig_pos * ct)
	* rotation(orig_rot * ct)
	* scale(ct + t * target_scale);

	post_redraw();
	++frame_count;
	return true;
}

void PuzzleFinish::draw()
{
	Interlude::shader.setTransform(transformation);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
