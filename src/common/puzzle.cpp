#include <utility>
#include <inttypes.h>
#include "piece.hpp"
#include "options.hpp"
#include "shader.hpp"
#include "math.hpp"

#include "puzzle.hpp"

using namespace std;

static CamShader shader;
static Uniform mask_val;
static Uniform alpha;

void Puzzle::initialize() {
	const char *sources[] = {"puzzle.frag", "default.vert", NULL};
	shader.setup_shader(sources);
	shader.enable();
	shader.getUniform("figure").set(0);
	shader.getUniform("mask").set(1);
	mask_val = shader.getUniform("mask_val");
	alpha = shader.getUniform("alpha");

	// I am standardizing the screen width to 2 and height to whatever the screen ratio allows
	// The puzzle images should have width of 1.5 and height of 1, ergo ratio of 3:2
	shader.setProjection(ortho(-1, 1, -Options::half_h, Options::half_h, -1, 1));
}

void Puzzle::setup_draw()
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	shader.enable();
}

bool Puzzle::update()
{
	finished = parts.size() == 1;
	return false;
}

void Puzzle::draw()
{
	figure.enable(GL_TEXTURE0);
	split.enable(GL_TEXTURE1);

	for(auto i = parts.rbegin(); i != parts.rend(); ++i) {
		(*i)->draw(shader, mask_val, alpha);
	}
}

bool Puzzle::done()
{
	return finished;
}

std::shared_ptr<Cluster> Puzzle::pick(float x, float y)
{
	for(auto i = begin(parts); i != end(parts); ++i) {
		auto& c = *i;
		if(c->contains(*mask_buf, x, y)) {
			// Found; put it in first position
			parts.push_front(move(*i));
			parts.erase(i);

			return parts.front();
		}
	}

	return nullptr;
}

bool Puzzle::tryMerge(Cluster* c)
{
	if(!c->tryMerge(mask_buf.get())) {
		return false;
	}

	// Held items are stored first, so this is almost O(1)...
	for(auto iter = begin(parts); iter != end(parts);  iter++) {
		if(iter->get() == c) {
			parts.erase(iter);
			break;
		}
	}

	// Rebuild texture from mask_buf, that might have changed...
	// TODO(very low priority): check if this is actually needed.
	split.setupFromBuffer(*mask_buf, GL_NEAREST);

	return true;
}
