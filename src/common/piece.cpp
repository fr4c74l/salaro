#include <algorithm>
#include "gl.hpp"
#include "piece.hpp"

using namespace std;

Piece::Piece(Vector2 p, Vector2 s, uint8_t mask):
	pos(p),
	size(s),
	color_mask(mask)
{
	glGenBuffers(1, &vbo);
	update_buffer();
}

void Piece::update_buffer()
{
	Vertex vert[4];

	float tex_bottom = 1.0f - (pos.y + 0.5f);
	float tex_top = 1.0f - (pos.y + 0.5f + size.y);
	float tex_left = (pos.x + 0.75f) / 1.5f;
	float tex_right = (pos.x + 0.75f + size.x) / 1.5f;

	vert[0].tex = Vector2(tex_left, tex_bottom);
	vert[0].pos = Vector2(pos.x, pos.y);

	vert[1].tex = Vector2(tex_right, tex_bottom);
	vert[1].pos = Vector2(pos.x + size.x, pos.y);

	vert[2].tex = Vector2(tex_right, tex_top);
	vert[2].pos = Vector2(pos.x + size.x, pos.y + size.y);

	vert[3].tex = Vector2(tex_left, tex_top);
	vert[3].pos = Vector2(pos.x, pos.y + size.y);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vert), vert, GL_STATIC_DRAW);
}

Piece::~Piece()
{
	glDeleteBuffers(1, &vbo);
}

void Piece::draw(Uniform mask)
{
	mask.set(color_mask);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)sizeof(Vector2));
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void Piece::merge(Piece *other, ImgBuffer& mask)
{
	int h = mask.getHeight();
	int w = mask.getWidth();
	int pitch = mask.getPitch();
	uint8_t* data = mask.getData();

	// Find limits of the other piece in the mask buffer,
	// increased by 5 pixels to account for possible FP errors:
	int x0 = max(0, int(other->pos.x / 1.5f * w) + w / 2 - 5);
	int y0 = max(0, int((1.0f - (other->pos.y + other->size.y + 0.5f)) * h) - 5);
	int x1 = min(w, int((other->pos.x + other->size.x) / 1.5f * w) + w / 2 + 5);
	int y1 = min(h, int((1.0f - other->pos.y + 0.5f) * h) + 5);

	// Take the pixels from the other piece
	for(int i = y0; i < y1; ++i) {
		for(int j = x0; j < x1; ++j) {
			if(data[i*pitch + j] == other->color_mask)
				data[i*pitch + j] = color_mask;
		}
	}

	// Calculate the new limits of this piece:
	Vector2 op = pos + size;
	Vector2 other_op = other->pos + other->size;

	pos.x = min(pos.x, other->pos.x);
	pos.y = min(pos.y, other->pos.y);

	size.x = max(op.x, other_op.x) - pos.x;
	size.y = max(op.y, other_op.y) - pos.y;
	update_buffer();
}
