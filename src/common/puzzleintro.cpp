#include <condition_variable>
#include "random.hpp"
#include "shader.hpp"
#include "native.hpp"
#include "math.hpp"
#include "options.hpp"
#include "interlude.hpp"

#include "puzzleintro.hpp"

using namespace std;

PuzzleIntro::PuzzleIntro(unique_ptr<Puzzle> &&target):
animation([&](std::function<void()> yeld){ animation_steps(yeld); }),
transform(Matrix4::IDENTITY),
draw_puzzle(false),
puzzle(std::move(target))
{}

void PuzzleIntro::setup_draw()
{
	Interlude::setup_draw();
	Interlude::alpha.set(1.0f);
	puzzle->figure.enable();
}

bool PuzzleIntro::update()
{
	animation.call();
	post_redraw();
	return true;
}

void PuzzleIntro::draw()
{
	if(draw_puzzle) {
		puzzle->draw();
	} else {
		Interlude::shader.setTransform(transform);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}
}

void PuzzleIntro::animation_steps(std::function<void()> yield)
{
	std::uniform_real_distribution<float> pos_shake(-0.02f, 0.02f);
	std::uniform_real_distribution<float> rot_shake(-2.0f * M_PI / 180.0f,
							2.0f * M_PI / 180.0f);

	// Wait for an interval
	auto wait = [&](int interval)
	{
		for(int i = 0; i < interval; ++i) {
			yield();
		}
	};
	
	// Shake
	auto shake = [&](int interval)
	{
		for(int i = 0; i < interval; ++i) {
			transform =
			translation(Vector2(pos_shake(Random::generator),
					    pos_shake(Random::generator)))
			* rotation(rot_shake(Random::generator))
			* transform;
			yield();
		}
	};

	// Waiting then shaking
	const int intervals[] = {90, 60, 40};
	for(int interval : intervals) {
		wait(interval);
		shake(12);
	}

	// TODO: Load puzzle asynchronously, wait for puzzle finish loading here...

	// Second successive shake:
	wait(7);
	shake(8);
	
	// Start drawing from puzzle:
	for(auto& c : puzzle->parts) {
		c->setTransform(transform);
	}
	draw_puzzle = true;
	puzzle->setup_draw();

	// Offset the pieces for the breaking appearance:
	for(int i = 0; i < 4; ++i) {
		for(auto& c : puzzle->parts) {
			c->pos += Vector2(pos_shake(Random::generator),
					  pos_shake(Random::generator));
			c->rot += rot_shake(Random::generator);
		}
		yield();
	}

	wait(15);

	// Attract all pieces to the center
	struct Scramble {
		Vector2 ref_pos;
		float ref_rot;
		float rot;
		Cluster* c;
	};
	vector<Scramble> scramble_state;

	{
		std::uniform_real_distribution<float> rand_rot(-3 * M_PI, 3 * M_PI);
		for(auto &c : puzzle->parts) {
			scramble_state.push_back(Scramble());
			Scramble& s = scramble_state.back();
			s.ref_pos = c->pos;
			s.ref_rot = c->rot;
			s.rot = rand_rot(Random::generator);
			s.c = c.get();
		}
	}

	static const int DURATION = 60;
	for(int t = 0; t < DURATION; ++t) {
		float time = t / float(DURATION);

		// Still 0 to 1, but sparser in the middle...
		float weighted = 0.5f - 0.5f * cosf(time * M_PI);
		float complement = 1.0f - weighted;

		for(auto& s : scramble_state) {
			// Moving to center
			s.c->pos = s.ref_pos * complement;

			// Spinning
			s.c->rot = s.ref_rot + s.rot * weighted;
		}

		yield();
	}

	// Send each piece to its last position before start:
	uniform_real_distribution<float> xrand(-1.0, 1.0),
	yrand(-Options::half_h, Options::half_h),
	quarter_arc(-0.5f * M_PI, 0.5f * M_PI);

	for(auto &s : scramble_state) {
		s.ref_pos = Vector2(xrand(Random::generator), yrand(Random::generator));
		s.ref_rot = s.c->rot;
		s.rot = quarter_arc(Random::generator);
	}

	for(int t = 0; t < DURATION; ++t) {
		float time = t / float(DURATION);

		// Abrupt start, slow ending:
		float weighted = cosf((time * 0.5f - 0.5f) * M_PI);
		for(auto& s : scramble_state) {
			// Moving out of center
			s.c->pos = s.ref_pos * weighted;

			// Spinning
			s.c->rot = s.ref_rot + s.rot * weighted;
		}
		yield();
	}
}
