#pragma once
#include "imgbuffer.hpp"

class PlainBuffer: public ImgBuffer {
public:
	PlainBuffer(uint16_t w, uint16_t h, uint8_t bpp):
		w(w), h(h), bpp(bpp), data(new uint8_t[w*h*bpp])
	{}

	virtual ~PlainBuffer()
	{}

	virtual uint8_t* getData() const
	{
		return data.get();
	}

	virtual uint8_t getBPP() const
	{
		return bpp;
	}

	virtual uint16_t getWidth() const
	{
		return w;
	}

	virtual uint16_t getHeight() const
	{
		return h;
	}

	virtual uint16_t getPitch() const
	{
		return w * bpp;
	}

private:
	uint16_t w, h;
	uint8_t bpp;
	std::unique_ptr<uint8_t[]> data;
};
