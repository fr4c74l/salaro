#pragma once

class Animator {
public:
	virtual ~Animator() {}
	virtual bool tick() = 0;
};