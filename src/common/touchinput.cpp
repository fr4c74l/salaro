#include <cassert>

#include "touchinput.hpp"

using namespace std;

void TouchInput::touch(int id, float x, float y)
{
	lock_guard<mutex> lock(enter_mutex);

	auto c = target->pick(x, y);
	shared_ptr<Pinch> pinch;

	auto insert_ret = cluster2pinch.insert(make_pair(c.get(), weak_ptr<Pinch>()));
	weak_ptr<Pinch>& wptr = insert_ret.first->second;
	if(wptr.expired()) {
		// New piece selected...
		pinch = make_shared<Pinch>(id, x, y, insert_ret.first, Hold(c));
		wptr = pinch;
		id2pinch[id] = make_pair(0, pinch);
	} else {
		// Another finger on a already selected piece...
		pinch = wptr.lock();
		assert(pinch->finger_mask <= 0x3);
		assert(pinch->finger_mask > 0);
		if(pinch->finger_mask != 0x3) {
			// Only one finger on piece, slot for one more.
			int idx = pinch->finger_mask & 1u;

			pinch->id[idx] = id;
			pinch->pos[idx] = Vector2(x, y);
			pinch->finger_mask |= 1u << idx;

			id2pinch[id] = make_pair(idx, pinch);
		} else {
			// Two fingers already pinching; ignore this touch.
		}
	}
}

void TouchInput::process_moves()
{
	lock_guard<mutex> lock(enter_mutex);

	// Process moves stored during the last frame
	for(auto i = next_moves.cbegin(); i != next_moves.cend(); next_moves.erase(i++)) {
		auto pinch_iter = id2pinch.find(i->first);
		if(pinch_iter != id2pinch.end()) {
			// Found associated pinch.
			int idx = pinch_iter->second.first;
			Pinch& pinch = *pinch_iter->second.second;

			assert(pinch.finger_mask <= 0x3);
			assert(pinch.finger_mask > 0);

			if(shared_ptr<Cluster> cluster = pinch.cluster.lock()) {
				if(pinch.finger_mask != 0x3) {
					// Only one finger, simple drag.
					Vector2 delta = i->second - pinch.pos[idx];
					cluster->move(delta.x, delta.y);
				} else {
					// Double finger, drag and rotate.
					int idx2 = !idx;

					// Find second finger position:
					auto j = next_moves.find(pinch.id[idx2]);
					if(j != next_moves.end()) {
						// Found, handle both points together...
						Vector2& pos2 = j->second;

						// Translation stuff:
						Vector2 prev_middle = (pinch.pos[idx] + pinch.pos[idx2]) * 0.5f;
						Vector2 new_middle = (i->second + pos2) * 0.5f;
						Vector2 delta = new_middle - prev_middle;
						cluster->move(delta.x, delta.y);

						// Rotation stuff:
						Vector2 prev_segment = pinch.pos[idx] - pinch.pos[idx2];
						Vector2 new_segment = i->second - pos2;
						float angle = atan2f(new_segment.y, new_segment.x)
							- atan2f(prev_segment.y, prev_segment.x);
						cluster->rotate(new_middle, angle);

						// Update second finger position:
						pinch.pos[idx2] = pos2;

						// Erase data so it won't be handled again:
						next_moves.erase(j);
					} else {
						// Second finger did not change, rotate around it...
						Vector2 prev_segment = pinch.pos[idx] - pinch.pos[idx2];
						Vector2 new_segment = i->second - pinch.pos[idx2];
						float angle = atan2f(new_segment.y, new_segment.x)
							- atan2f(prev_segment.y, prev_segment.x);
						cluster->rotate(pinch.pos[idx2], angle);
					}
				}
			}

			// Update first finger position:
			pinch.pos[idx] = i->second;
		}
	}

	// Try to merge the clusters released in the last frame.
	for(Hold& h : to_try_merge) {
		if(auto cluster = h.lock())
			target->tryMerge(cluster.get());
	}
	to_try_merge.clear();
}

void TouchInput::lift(int id)
{
	lock_guard<mutex> lock(enter_mutex);

	auto pinch_iter = id2pinch.find(id);
	if(pinch_iter != id2pinch.end()) {
		int idx = pinch_iter->second.first;
		Pinch& pinch = *pinch_iter->second.second;

		assert(pinch.finger_mask <= 0x3);
		assert(pinch.finger_mask > 0);

		// Remove finger from pinch:
		pinch.finger_mask &= ~(1u << idx);

		if(pinch.finger_mask == 0) {
			// This pinch itself will be erased,
			// so remove the entry pointing to it in cluster index...
			cluster2pinch.erase(pinch.iter);

			// ...and schedule to try to merge its cluster.
			to_try_merge.push_back(move(pinch.cluster));
		}

		id2pinch.erase(pinch_iter);
	}
}

void TouchInput::reset()
{
	cluster2pinch.clear();
	id2pinch.clear();
	next_moves.clear();
}

TouchInput::Pinch::Pinch(int first_id, float first_x, float first_y, ClusterIndex::iterator i, Hold&& c):
iter(i),
cluster(std::move(c)),
finger_mask(1u)
{
	id[0] = first_id;
	pos[0] = Vector2(first_x, first_y);
}
