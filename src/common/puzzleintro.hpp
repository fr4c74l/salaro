#pragma once

#include <functional>
#include <future>
#include <memory>
#include "context.hpp"
#include "coroutine.hpp"
#include "matrix4.hpp"

#include "puzzle.hpp"
#include "texture.hpp"

class PuzzleIntro: public Context
{
public:
	PuzzleIntro(std::unique_ptr<Puzzle> &&target);

	void setup_draw();

	bool update();
	void draw();

	bool done() {
		return animation.done();
	}

	std::unique_ptr<Puzzle> getPuzzle() {
		return std::move(puzzle);
	}
private:
	void animation_steps(std::function<void()> yield);
	CoRoutine animation;

	Matrix4 transform;

	std::unique_ptr<Puzzle> puzzle;

	bool draw_puzzle;
};
