#include "matrix4.hpp"
#include "vector2.hpp"
#include "math.hpp"

Matrix4
ortho(float left, float right, float bottom, float top, float near, float far)
{
	float a = 2.0f / (right - left);
	float b = 2.0f / (top - bottom);
	float c = -2.0f / (far - near);
	
	float tx = - (right + left)/(right - left);
	float ty = - (top + bottom)/(top - bottom);
	float tz = - (far + near)/(far - near);
	
	return Matrix4 (
		a, 0, 0, tx,
		 0, b, 0, ty,
		 0, 0, c, tz,
		 0, 0, 0, 1
	);
}

Matrix4
rotation(float angle) {
  const float c = cos(angle);
  const float s = sin(angle);
  return Matrix4(c, -s, 0, 0,
                 s, c, 0, 0,
                 0, 0, 1, 0,
                 0, 0, 0, 1);
}

Matrix4
translation(Vector2 delta)
{
	return Matrix4(
			1.0f, 0.0f, 0.0f, delta.x,
			0.0f, 1.0f, 0.0f, delta.y,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
			);
}

Matrix4
scale(Vector2 size)
{
	return Matrix4(
			size.x, 0.0f, 0.0f, 0.0f,
			0.0f, size.y, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
			);
}

Matrix4
scale(float size)
{
	return Matrix4(
		size, 0.0f, 0.0f, 0.0f,
		0.0f, size, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

Vector2 operator/(float s, const Vector2& v)
{
	return Vector2(s / v.x, s / v.y);
}

float normalize_rot(float r)
{
	float spins = r / (2.0f*M_PI);
	float whole_spins;
	float fraction = modff(spins, &whole_spins);

	if(fraction > 0.5f)
		fraction -= 1.0f;
	else if(fraction < -0.5f)
		fraction += 1.0f;

	return fraction * 2.0f * M_PI;
}
