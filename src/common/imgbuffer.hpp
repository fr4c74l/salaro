#pragma once

#include <inttypes.h>
#include <iostream>
#include <cassert>
#include "gl.hpp"

class ImgBuffer {
public:
	virtual ~ImgBuffer() = 0;

	virtual uint8_t* getData() const = 0;

	virtual uint8_t getBPP() const = 0;
	virtual uint16_t getWidth() const = 0;
	virtual uint16_t getHeight() const = 0;
	virtual uint16_t getPitch() const = 0;

	virtual GLint getAlignment() const;

	inline friend std::ostream& operator <<
		( std::ostream& o, const ImgBuffer& img)
	{
		assert(img.getBPP() != 2);
		assert(img.getBPP() != 4);
		o << (img.getBPP() == 3 ? "P3\n" : "P2\n") << img.getWidth() << ' ' << img.getHeight() << "\n255\n";
		for (size_t i = 0; i <  img.getHeight(); ++i)
		{
			for(size_t j = 0; j <  img.getWidth(); ++j)
			{
				for(size_t k = 0; k < img.getBPP(); ++k)
					o <<  (int)img.getData()[(i * img.getPitch() + j * img.getBPP()) + k] << ' ';
			}
			o << '\n';
		}
		return o;
	}
};
