#pragma once

#include "inputhandler.hpp"
#include "hold.hpp"
#include <boost/concept_check.hpp>

class MouseInput: public InputHandler
{
public:
	void hold(float x, float y, bool to_rot);
	void drag(float x, float y);
	void turn(float x, float y, bool to_right);

	void release();

protected:
	void reset();

private:
	Hold selected;
	Vector2 hold_point;
	float last_rot_drag_angle;
	bool selection_is_rot;

	std::weak_ptr<Cluster> last_rot;
	Vector2 last_rot_pos;
};