#pragma once

class Context
{
public:
	virtual ~Context() {}

	virtual void setup_draw() = 0;

	/**
	 * @returns True, if updated should be called again next frame, false if there is nothing to update next frame.
	 */
	virtual bool update() = 0;
	virtual void draw() = 0;

	virtual bool done() = 0;
};