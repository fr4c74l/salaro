#pragma once

#include <map>
#include <unordered_map>
#include <memory>
#include <mutex>
#include "vector2.hpp"
#include "hold.hpp"
#include "inputhandler.hpp"

class TouchInput: public InputHandler
{
public:
	void touch(int id, float x, float y);
	void process_moves();
	void lift(int id);

	void inform_move(int id, float x, float y)
	{
		std::lock_guard<std::mutex> lock(enter_mutex);
		next_moves[id] = Vector2(x, y);
	}

protected:
	void reset();

private:
	struct Pinch;
	typedef std::map<Cluster*, std::weak_ptr<Pinch>> ClusterIndex;

	struct Pinch
	{
		Pinch(int first_id, float first_x, float first_y,
		      ClusterIndex::iterator i, Hold&& c);
		int id[2];
		Vector2 pos[2];
		ClusterIndex::iterator iter;
		Hold cluster;
		unsigned char finger_mask;
	};

	std::unordered_map<int, Vector2> next_moves;
	std::unordered_map<int, std::pair<char, std::shared_ptr<Pinch>>> id2pinch;
	ClusterIndex cluster2pinch;

	std::vector<Hold> to_try_merge;

	std::mutex enter_mutex;
};
