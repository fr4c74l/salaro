#pragma once

#include "gl.hpp"
#include "shader.hpp"

namespace Interlude
{

extern CamShader shader;
extern GLuint vbo;
extern Uniform alpha;

void initialize();
void setup_draw();

}