#include <cstdlib>
#include "coroutine.hpp"

const size_t CoRoutine::STACK_SIZE = 8*1024*1024;

void CoRoutine::stack_allocate()
{
	void *ptr = std::malloc(STACK_SIZE);
	if(!ptr)
		throw std::bad_alloc();

        coroutine_stack = static_cast<char*>(ptr) + STACK_SIZE;
}

void CoRoutine::stack_deallocate()
{
	void *ptr = static_cast<char*>(coroutine_stack) - STACK_SIZE;
	std::free(ptr);
	coroutine_stack = nullptr;
}

