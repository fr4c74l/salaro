#include <vector>
#include "imgbuffer.hpp"

ImgBuffer::~ImgBuffer()
{}

GLint ImgBuffer::getAlignment() const
{
	int min = ((unsigned long) getData()) % 8;
	int max = min + getPitch();
	int val = min | max | 8;

	return val & (~val+1);
	// I don't really believe this code will ever be compiled to some
	// machine out there that doesn't use two's complemet as negative
	// representation; but for the sake of correctness, I will not use
	// (val & -val), which is undefined by C++ specification.
}
