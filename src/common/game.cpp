#include <memory>
#include <list>
#include "native.hpp"
#include "puzzle.hpp"
#include "options.hpp"
#include "animator.hpp"
#include "config.hpp"
#include "puzzleintro.hpp"
#include "puzzlefinish.hpp"
#include "puzzletransition.hpp"
#include "coroutine.hpp"
#include "interlude.hpp"
#include "mouseinput.hpp"
#include "splitters/rect.hpp"
#include "splitters/imgcutter.hpp"

#include "game.hpp"

using namespace std;

namespace Game
{

static unique_ptr<Context> active;

static list<shared_ptr<Animator> > anims;

template <typename T, size_t N>
static inline size_t
array_size( const T(&)[ N ] )
{
	return N;
}

// Sequence of levels
static CoRoutine game_sequence([](std::function<void()> run_level)
{
	const char* levels[] = {"level1.jpg", "level2.jpg"};
	const char* splits[] = {"split1.png", "split2.png"};
	int i = 0;

	// First introductory animation
	PuzzleIntro* intro;
    {
        auto first_puzzle = new Puzzle(*new_image_from_file(levels[i]), Splitter::ImgCutter(splits[i]));
        ++i;
        intro = new PuzzleIntro(unique_ptr<Puzzle>(first_puzzle));
    }
	active.reset(intro);
	active->setup_draw();
	run_level();

	Puzzle *puzzle;
	while(true) {
		// Level running
		{
			unique_ptr<Puzzle> next(std::move(intro->getPuzzle()));
			input_set_puzzle(puzzle = next.get());
			active = std::move(next);
		}
		// No need to call setup_draw here, because PuzzleIntro
		// have already called it.
		//active->setup_draw();

		run_level();

		if(i >= array_size(levels))
			break;

		// Transition to next level
		PuzzleTransition* trans;
		active.reset(trans =
		new PuzzleTransition(*puzzle,
				     unique_ptr<Puzzle>(
					     new Puzzle(*new_image_from_file(levels[i]),
							Splitter::ImgCutter(splits[i]))),
				     Vector2(((1411.0*1.5) / 2592.0)-0.75,
					     0.5 - ((402.0+246.5) / 1728.0)),
				     740.0 /2592.0));
        i++;
		input_set_puzzle(nullptr);

		active->setup_draw();
		run_level();

		// Introduction of next level
		active.reset(intro = new PuzzleIntro(trans->getPuzzle()));
		active->setup_draw();
		run_level();
	}

	// Final animation
	active.reset(new PuzzleFinish(*puzzle));
    active->setup_draw();
	input_set_puzzle(puzzle = nullptr);

	run_level();
});

bool
update()
{
	// Animation stuff
	if(!anims.empty())
		post_redraw();

	anims.remove_if([](const shared_ptr<Animator>& e) {
		return !e->tick();
	});

	// Go to next level (or whatever), if needed and possible...
	if(active->done() && !game_sequence.done()) {
		game_sequence.call();
		active->setup_draw();
		post_redraw();
	}
	
	// General update
	bool ret = active->update();
	return ret || !anims.empty();
}

void
draw()
{
	glClear(GL_COLOR_BUFFER_BIT);
	active->draw();
	glFlush();
}

void
initialize()
{
	// OpenGL nonchanging settings
	glClearColor(0.8f, 0.8f, 0.7f, 0.0f);

	// Differences between OpenGL and OpenGL ES:
#ifndef GL_ES_VERSION_2_0
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_TEXTURE_2D);
#endif

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glViewport(0, 0, Options::width, Options::height);

	glEnableVertexAttribArray(0); // Position
	glEnableVertexAttribArray(1); // Texture coord

	glClear(GL_COLOR_BUFFER_BIT);

	Puzzle::initialize();
	Interlude::initialize();

	game_sequence.call();
}

void add_animation(shared_ptr<Animator> anim)
{
	anims.push_back(move(anim));
}

}
