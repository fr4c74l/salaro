#include <algorithm>
#include "game.hpp"
#include "options.hpp"
#include "native.hpp"
#include "math.hpp"
#include "random.hpp"
#include "shader.hpp"

#include "cluster.hpp"

using namespace std;

Cluster::Cluster(Piece *p):
	pos(0.0f, 0.0f),
	rot(0.0f),
	offset(0.0f, 0.0f),
	alpha(1.0f)
{
	add(p);
	pos = offset;
}

Cluster::~Cluster()
{
	for(Piece* p : pieces) {
		delete p;
	}
}

void Cluster::setTransform(const Matrix4& from)
{
	Vector4 p(pos.x, pos.y, 0.0f, 1.0f);
	p = from * p;
	pos = Vector2(p.x, p.y);
	rot = -atan2f(from[0][1], from[0][0]);
}

void Cluster::addNeighbor(Cluster* other)
{
	vicinity.insert(other);
	other->vicinity.insert(this);
}

void Cluster::draw(Shader& s, Uniform mask, Uniform transparency)
{
	s.setTransform(transform());
	transparency.set(alpha);

	for(Piece* p : pieces) {
		p->draw(mask);
	}
}

bool Cluster::contains(const ImgBuffer& mask_buf, float x, float y)
{
	Vector4 p = inverseTransform() * Vector4(x, y, 0.0f, 1.0f);
	uint16_t w = mask_buf.getWidth();
	uint16_t h = mask_buf.getHeight();
	int ix = int(((p.x + 0.75f) / 1.5f) * w);
	int iy = int((1.0f - (p.y + 0.5f)) * h);

	if(ix < 0 || ix >= w || iy < 0 || iy >= h)
		return false;

	uint8_t color = mask_buf.getData()[iy*mask_buf.getPitch() + ix];

	for(Piece* p : pieces) {
		if(p->getMask() == color)
			return true;
	}
	return false;
}

void Cluster::move(float x, float y)
{
	pos = pos + Vector2(x, y);
	pos.x = std::max(std::min(pos.x, 1.0f), -1.0f);
	pos.y = std::max(std::min(pos.y, Options::half_h), -Options::half_h);
	post_redraw();
}

void Cluster::rotate(const Vector2& origin, float angle)
{
	rot += angle;
	if(rot > M_PI) {
		rot -= 2.0f * M_PI;
	} else if(rot < - M_PI) {
		rot += 2.0f * M_PI;
	}

	Vector2 rel_pos = pos - origin;
	float s = sin(angle);
	float c = cos(angle);
	Vector2 rotated(rel_pos.x * c - rel_pos.y * s,
			rel_pos.x * s + rel_pos.y * c);
	Vector2 delta = rotated - rel_pos;

	move(delta.x, delta.y);
}

void Cluster::setAlpha(float alpha)
{
	auto ptr = alpha_anim.lock();
	if(ptr) {
		ptr->reset(alpha);
	} else {
		ptr.reset(new AlphaAnim(shared_from_this(), alpha));
		Game::add_animation(ptr);
		alpha_anim = ptr;
	}
}

void Cluster::add(Piece* piece)
{
	pieces.push_back(piece);
	calculateOffset();
}

Cluster* Cluster::tryMerge(ImgBuffer* mask)
{
	for(auto iter = vicinity.begin(); iter != vicinity.end(); ++iter) {
		// Adjust these values to tune contact detection...
		// "delta" affects both rotation and translation,
		// while "radius" affects how rotation relates to translation
		// (smaller the radius, greater the distance a piece will attach to other).
		static const float delta = 0.01f;
		static const float radius = 0.05f;
	
		Cluster* c = *iter;
		Matrix4 t = translation(offset - c->offset) * rotation(-rot) * translation(c->pos - pos) * rotation(c->rot);
	
		Vector4 p1 = t * Vector4(-radius, 0.0f, 0.0f, 1.0f);
		Vector4 p2 = t * Vector4(radius, 0.0f, 0.0f, 1.0f);
		if(fabsf(p1.x + radius) < delta && fabsf(p2.x - radius) < delta && fabsf(p1.y) < delta && fabsf(p2.y) < delta) {
			c->merge(this, mask);
			post_redraw();
			return c;
		}
	}

	return nullptr;
}

void Cluster::merge(Cluster* other, ImgBuffer* mask)
{
	vicinity.erase(other);
	other->vicinity.erase(this);
	for(Cluster* c : other->vicinity) {
		c->vicinity.erase(other);
		addNeighbor(c);
	}
	other->vicinity.clear();

	if(mask) {
		// Merge pieces by pixel
		assert(!pieces.empty());
		Piece *ours = pieces[0];
		for(Piece *p : other->pieces) {
			ours->merge(p, *mask);
		}
	} else {
		// Glue independent parts
		pieces.insert(end(pieces), begin(other->pieces), end(other->pieces));
	}
	other->pieces.clear();

	calculateOffset();
}

void Cluster::calculateOffset()
{
	float top = -100.0f, bottom = 100.0f, left = 100.0f, right = -100.0f;

	for(Piece* p : pieces) {
		top = max(top, p->top());
		right = max(right, p->right());
		bottom = min(bottom, p->bottom());
		left = min(left, p->left());
	}

	Vector4 newOffset = Vector4((left + right) * 0.5f, (bottom + top) * 0.5f, 0.0f, 1.0f);
	Vector4 displace = rotation(rot) * (newOffset - Vector4(offset.x, offset.y, 0.0f, 1.0f));
	pos = pos + Vector2(displace.x, displace.y);
	offset.x = newOffset.x;
	offset.y = newOffset.y;
}

Matrix4 Cluster::transform()
{
	// TODO: Cache result...
	return translation(pos) * rotation(rot) * translation(-offset);
}

Matrix4 Cluster::inverseTransform()
{
	// TODO: Cache result...
	return translation(offset) * rotation(-rot) * translation(-pos);
}

bool Cluster::ClusterAnim::tick()
{
	if(auto c = cluster.lock())
		return tick_impl(c);
	else
		return false;
}

void Cluster::AlphaAnim::reset(float t)
{
	target = t;
	if(auto c = cluster.lock()) {
		if(c->alpha < t) {
			increment = 1.0f/20.0f;
		} else {
			increment = -1.0f/20.0f;
		}
	}
}

bool Cluster::AlphaAnim::tick_impl(const shared_ptr<Cluster>& c)
{
	float tmp = c->alpha + increment;
	if(increment * tmp < increment * target) {
		c->alpha = tmp;
		return true;
	} else {
		c->alpha = target;
		return false;
	}
}
