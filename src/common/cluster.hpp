#pragma once

#include <memory>
#include <vector>
#include <unordered_set>
#include "matrix4.hpp"
#include "vector2.hpp"
#include "piece.hpp"
#include "shader.hpp"
#include "imgbuffer.hpp"
#include "animator.hpp"

class PuzzleIntro;

class Cluster: public std::enable_shared_from_this<Cluster>
{
public:
	Cluster(Piece* piece);
	~Cluster();

	void addNeighbor(Cluster* other);

	void draw(Shader& s, Uniform mask, Uniform alpha);

	bool contains(const ImgBuffer& mask_buf, float x, float y);
	void move(float x, float y);
	void rotate(const Vector2& origin, float angle);
	void setAlpha(float alpha);

	void add(Piece* piece);
	Cluster* tryMerge(ImgBuffer* mask_buf);

	const Vector2& getPos() const
	{
		return pos;
	}

	const float getRot() const
	{
		return rot;
	}

private:
	void setTransform(const Matrix4 &from);

	void merge(Cluster* other, ImgBuffer* mask_buf);
	void calculateOffset();

	Matrix4 transform();
	Matrix4 inverseTransform();

	std::vector<Piece*> pieces;

	std::unordered_set<Cluster*> vicinity;

	/** Offset relative to original internal pieces position. */
	Vector2 offset;

	/** On screen position. */
	Vector2 pos;
	float rot;

	/** Transparency when selected. */
	float alpha;

	class ClusterAnim: public Animator {
	public:
		ClusterAnim(std::weak_ptr<Cluster>&& c):
		cluster(std::move(c))
		{}

		virtual ~ClusterAnim()
		{}

		bool tick();
	protected:
		virtual bool tick_impl(const std::shared_ptr<Cluster>& c) = 0;
		std::weak_ptr<Cluster> cluster;
	};

	/** Transparency animation class. */
	class AlphaAnim: public ClusterAnim {
	public:
		AlphaAnim(std::weak_ptr<Cluster>&& c, float t):
		ClusterAnim(std::move(c))
		{
			reset(t);
		}

		void reset(float t);

	protected:
		bool tick_impl(const std::shared_ptr<Cluster>& c);

	private:
		float increment;

		float target;
	};
	std::weak_ptr<AlphaAnim> alpha_anim;

	friend class PuzzleIntro;
};
