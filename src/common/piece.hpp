#pragma once

#include <set>
#include <vector>
#include "vertex.hpp"
#include "vector2.hpp"
#include "matrix4.hpp"
#include "shader.hpp"
#include "imgbuffer.hpp"

class Piece
{
public:
	Piece(Vector2 origin, Vector2 size, uint8_t color_mask);
	~Piece();
	void draw(Uniform mask);

	void merge(Piece* other, ImgBuffer& mask);

	uint8_t getMask() {
		return color_mask;
	}

	float top()
	{
		return pos.y + size.y;
	}

	float bottom()
	{
		return pos.y;
	}

	float left()
	{
		return pos.x;
	}

	float right()
	{
		return pos.x + size.x;
	}

private:
	void update_buffer();

	GLuint vbo;

	Vector2 pos;
	Vector2 size;
	uint8_t color_mask;
};
