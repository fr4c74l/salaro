#pragma once

#include <cstdlib>
#include <ostream>
#include <cmath>

class Vector2
{
public:
	Vector2()
	{}

	Vector2(float xl, float yl)
	{
		x = xl;
		y = yl;
	}

	float& operator[](size_t elem)
	{
		return v[elem];
	}

	Vector2 operator+(const Vector2& o) const
	{
		return Vector2(x + o.x, y + o.y);
	}

	Vector2 operator+(float s) const
	{
		return Vector2(x + s, y + s);
	}

	Vector2& operator+=(const Vector2& o)
	{
		x += o.x;
		y += o.y;
		return *this;
	}

	Vector2 operator-() const
	{
		return Vector2(-x, -y);
	}

	Vector2 operator-(const Vector2& o) const
	{
		return Vector2(x - o.x, y - o.y);
	}

	Vector2 operator*(float s) const
	{
		return Vector2(x * s, y * s);
	}

	Vector2 operator*(const Vector2& o) const
	{
		return Vector2(x * o.x, y * o.y);
	}

	bool operator==(const Vector2& o) const
	{
		return x == o.x && y == o.y;
	}

	bool operator!=(const Vector2& o) const
	{
		return x != o.x || y != o.y;
	}

	float dot(const Vector2& o) const
	{
		return x * o.x + y * o.y;
	}

	float squared_length() const
	{
		return x*x + y*y;
	}

	float length() const
	{
		return sqrtf(squared_length());
	}

	const float* getVertex() const {return v;}

	/** Function for writing to a stream. */
	inline friend std::ostream& operator << ( std::ostream& o, const Vector2& v )
	{
		return o << '(' << v.x << ", " << v.y << ')';
	}

	// STL-like interface
	typedef float* iterator;

	iterator begin() {
		return v;
	}

	iterator end() {
		return &v[2];
	}

	union {
		struct {
			float x;
			float y;
		};
		float v[2];
	};
};
