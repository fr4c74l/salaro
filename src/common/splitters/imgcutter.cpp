#include <string.h>
#include <deque>
#include <vector>
#include <algorithm>
#include <functional>
#include <cassert>
#include <memory>
#include "config.hpp"
#include "plainbuffer.hpp"
#include "random.hpp"
#include "native.hpp"

#include "imgcutter.hpp"

using namespace std;

namespace Splitter
{

ImgCutter::ImgCutter(const std::string& filename):
mask(new_image_from_file(filename))
{
	if(mask->getBPP() != 1)
		mask = coalesce_RGB(*mask);
}

std::unique_ptr< ImgBuffer > ImgCutter::operator()(std::list< std::shared_ptr< Cluster > > &pieces)
{
	uint8_t* img = mask->getData();
	uint16_t w = mask->getWidth();
	uint16_t h = mask->getHeight();
	uint16_t pitch = mask->getPitch();

	// First of all, enforce we have a black-white buffer:
	for(uint16_t i = 0; i < h; ++i) {
		for(uint16_t j = 0; j < w; ++j) {
			uint8_t& byte = img[i*pitch + j];
			byte = (byte < 127) ? 0 : 255;
		}
	}

	// There is a hard limit of 254 possible different pieces in the puzzle.
	uint8_t found = 0;

	// Pixel position
	struct Pixel {
		Pixel(){}
		Pixel(uint16_t x, uint16_t y):
		x(x),
		y(y)
		{}
		uint16_t x;
		uint16_t y;
	};

	// Ideally, this should be a method of Pixel, but C++ won't allow me access to variables w and h ...
	auto for_neighborhood = [&] (Pixel p, std::function<void(Pixel)> func) {
		if(p.x > 0)
			func(Pixel(p.x-1, p.y));
		if(p.x+1 < w)
			func(Pixel(p.x+1, p.y));
		if(p.y > 0)
			func(Pixel(p.x, p.y-1));
		if(p.y+1 < h)
			func(Pixel(p.x, p.y+1));
	};

	// Properties of a piece's delimited area
	struct Area {
		Area(uint16_t w, uint16_t h) {
			min_w = w;
			max_w = 0;
			min_h = h;
			max_h = 0;
			pix_count = 0;
		}
		uint16_t min_w;
		uint16_t max_w;
		uint16_t min_h;
		uint16_t max_h;
		int pix_count;
		void extend(Pixel p) {
			min_w = min(min_w, p.x); max_w = max(max_w, p.x);
			min_h = min(min_h, p.y); max_h = max(max_h, p.y);
			++pix_count;
		}
	};
	vector<Area> found_lims;

	// Search the entire image for a blank pixel,
	// to start painting a new piece.
	for(uint16_t i = 0; i < h; ++i) {
		for(uint16_t j = 0; j < w; ++j) {
			if(img[i*pitch + j] == 255u) {
				// Found a new piece, paint it;
				++found;
				assert(found > 0 && "Can't handle more than 254 pieces...");

				found_lims.emplace_back(w, h);
				Area &l = found_lims.back();

				deque<Pixel> queue;
				queue.push_back(Pixel(j, i));

				// Wide search for pixels:
				do {
					Pixel& p = queue.front();
					if(img[p.y*pitch + p.x] != found) {
						if(img[p.y*pitch + p.x] == 255u) {
							for_neighborhood(p, [&] (Pixel n) {
								queue.push_back(n);
							});
						}
						img[p.y*pitch + p.x] = found;
						l.extend(p);
					}
					queue.pop_front();
				} while(!queue.empty());
			}
		}
	}

	// Annex any remaining black pixels (the shape divisors) to a neighbor piece
	bool done = false;
	do {
		// Find all black pixels neighboring a colored one
		vector<Pixel> to_paint;
		vector<Pixel> neighbors;
		for(uint16_t i = 0; i < h; ++i) {
			for(uint16_t j = 0; j < w; ++j) {
				if(img[i*pitch + j] == 0u) {
					neighbors.resize(to_paint.size() + 1);

					Pixel p(j, i);
					bool chosen = false;

					for_neighborhood(p, [&] (Pixel n) {
						if(img[n.y*pitch + n.x] != 0u) {
							neighbors.back() = n;
							chosen = true;
						}
					});

					if(chosen)
						to_paint.push_back(p);
				}
			}
		}

		// Paint the pixel with a neighboring color
		for(int i = 0; i < to_paint.size(); ++i) {
			Pixel p = to_paint[i];
			Pixel n = neighbors[i];

			uint8_t color = img[n.y*pitch + n.x];

			// Paint with chosen color
			img[p.y*pitch + p.x] = color;

			// Adjust the limits of the piece of that color
			found_lims[color-1].extend(p);
		}

		done = to_paint.empty();
	} while(!done);

	// Count how many pixels of each different colors each area neighbors
	vector<vector<int> > adj(found, vector<int>(found, 0)); // Adjacency matrix.
	for(uint16_t i = 0; i < h; ++i) {
		for(uint16_t j = 0; j < w; ++j) {
			Pixel p(j, i);
			for_neighborhood(p, [&] (Pixel n) {
				uint8_t p_idx = img[p.y*pitch + p.x] - 1;
				uint8_t n_idx = img[n.y*pitch + n.x] - 1;
				if(p_idx != n_idx) {
					adj[p_idx][n_idx]++;
				}
			});
		}
	}

	// Build the pieces:
	vector<Cluster *> built;
	const Vector2 norm(1.5f / (w-1), 1.0f / (h-1));
	const Vector2 disp(-0.75, -0.5);
	for(uint8_t i = 0; i < found; ++i) {
		Area &a = found_lims[i];
		Vector2 pos = Vector2(a.min_w, h-a.max_h-1) * norm + disp;
		Vector2 size = Vector2(a.max_w-a.min_w+1, a.max_h-a.min_h+1) * norm;
		built.push_back(new Cluster(new Piece(pos, size, i+1)));
	}

	// Add the vicinity:
	for(uint8_t i = 0; i < found; ++i) {
		for(uint8_t j = i; j < found; ++j) {
			if(adj[i][j] > 0) {
				built[i]->addNeighbor(built[j]);
			}
		}
	}

	// Since order matters, shuffle it:
	shuffle(begin(built), end(built), Random::generator);

	//TODO: Cluster together very small areas, useful for randomly generated partitions...

	// Build final piece list:
	for(auto& c: built) {
		pieces.emplace_front(std::move(c));
	}
	return std::move(mask);
}

unique_ptr<ImgBuffer> ImgCutter::coalesce_RGB(ImgBuffer& in)
{
	uint16_t w = in.getWidth();
	uint16_t h = in.getHeight();

	uint16_t in_pitch = in.getPitch();
	uint8_t in_bpp = in.getBPP();
	uint8_t in_channels = min(uint8_t(3u), in_bpp);
	uint8_t* in_data = in.getData();
	bool has_alpha = in_bpp == 4;

	unique_ptr<ImgBuffer> out(new PlainBuffer(w, h, 1));
	uint16_t out_pitch = out->getPitch();
	uint8_t* out_data = out->getData();

	for(int i = 0; i < h; ++i) {
		for(int j = 0; j < w; ++j) {
			unsigned int sum = 0;
			unsigned int alpha;
			if(has_alpha)
				alpha = in_data[i * in_pitch + j * in_bpp + 3];

			for(int k = 0; k < in_channels; ++k) {
				unsigned int tmp = in_data[i * in_pitch + j * in_bpp + k];

				// With alpha, background is taken as white.
				if(has_alpha)
					tmp = tmp * alpha / 255u + 255u - alpha;

				sum += tmp;
	
			}
			out_data[i * out_pitch + j] = sum / in_channels;
		}
	}

	return out;
}

}
