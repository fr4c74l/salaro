#pragma once

#include <cstdlib>
#include <list>
#include <memory>
#include <cassert>
#include "cluster.hpp"

namespace Splitter

{

class Rect
{
public:
	Rect(size_t c, size_t r):
	cols(c),
	rows(r)
	{
		assert((cols * rows) <= 254);
	}
	
	std::unique_ptr<ImgBuffer>
	operator()(std::list< std::shared_ptr< Cluster > > &parts);

private:
	size_t cols;
	size_t rows;
};

}
