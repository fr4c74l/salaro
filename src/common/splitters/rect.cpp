#include <cstdlib>
#include <memory>
#include "plainbuffer.hpp"
#include "cluster.hpp"

#include "rect.hpp"

namespace Splitter

{

std::unique_ptr<ImgBuffer> Rect::operator()(std::list< std::shared_ptr< Cluster > > &parts)
{
	std::vector<Cluster*> grid(cols * rows);

	float width = 1.5f / cols;
	float height = 1.0f / rows;

	// TODO: use screen resolution here:
	uint16_t mw = 800;
	uint16_t mh = 600;
	std::unique_ptr<PlainBuffer> mask(new PlainBuffer(mw, mh, 1));
	uint8_t* mdata = mask->getData();
	uint16_t mpitch = mask->getPitch();

	for(uint16_t y1 = mh, i = 0; i < rows; ++i) {
		uint16_t x0 = 0;
		uint16_t y0 = (rows - i - 1) * mh / rows;
		for(uint16_t j = 0; j < cols; ++j) {
			int idx = i * cols + j;
			uint8_t mask = idx + 1;
			auto& elem = grid[idx];
			elem = new Cluster(
				new Piece(Vector2(width * j - 0.75f, height * i - 0.5f),
					  Vector2(width, height), mask));

			uint16_t x1 = (j + 1) * mw / cols;
			for(uint16_t y = y0; y < y1; ++y) {
				for(uint16_t x = x0; x < x1; ++x) {
					mdata[y * mpitch + x] = mask;
				}
			}
			x0 = x1;

			parts.emplace_front(elem);
		}
		y1 = y0;
	}

	for(int i = 0; i < rows; ++i) {
		for(int j = 0; j < cols; ++j) {
			int idx = i * cols + j;
			auto& elem = grid[idx];
			if(i-1 >= 0)
				elem->addNeighbor(grid[idx - cols]);
			if(i+1 < rows)
				elem->addNeighbor(grid[idx + cols]);
			if(j-1 >= 0)
				elem->addNeighbor(grid[idx - 1]);
			if(j+1 < cols)
				elem->addNeighbor(grid[idx + 1]);
		}
	}

	return std::move(mask);
}

}
