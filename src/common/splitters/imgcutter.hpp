#pragma once

#include <inttypes.h>
#include <list>
#include "cluster.hpp"

namespace Splitter {

class ImgCutter
{
public:
	ImgCutter(const std::string& filename);
	std::unique_ptr<ImgBuffer> operator()(std::list<std::shared_ptr<Cluster> > &pieces);

private:
	/** Mix RGBA channels in a single greyscale channel based on RGB channels. */
	static std::unique_ptr<ImgBuffer> coalesce_RGB(ImgBuffer& in);

	std::unique_ptr<ImgBuffer> mask;
};

}
