#include <iostream>
#include <cstring>
#include <string>
//#include <android/log.h>
#include "config.hpp"
#include "native.hpp"

#include "shader.hpp"

using namespace std;

struct ltstr
{
  bool operator()(const char* s1, const char* s2) const
  {
    return strcmp(s1, s2) < 0;
  }
};
static std::map<const char*, GLuint, ltstr> loaded_shaders;

Shader::Shader():
	prog(0)
{}

Shader::Shader(const char *sources[])
{
	setup_shader(sources);
}

Shader::~Shader()
{
	if(prog)
		glDeleteProgram(prog);
}

void Shader::setup_shader(const char *sources[])
{
	char *ptr;
	const char **iter;
	const char *name;
	int ret;

	assert(sources);
	assert(*sources);
	prog = glCreateProgram();

	iter = sources;
	while(*iter != NULL)
	{
		GLint type;
		GLuint shader;

		name = *iter;
		if(loaded_shaders.find(name) == loaded_shaders.end())
		{
			if(strrchr(name, '.')[1] == 'v')
			{
				type = GL_VERTEX_SHADER;
			}
			else
			{
				type = GL_FRAGMENT_SHADER;
			}

			vector<char> code(read_file(string(DATA_DIR) + "shaders/" + name));
			const GLchar* ptr = &code[0];
			const GLint len = code.size();

			shader = glCreateShader(type);
			glShaderSource(shader, 1, &ptr, &len);
			glCompileShader(shader);
			{
				GLint status;
				glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
				if(!status) {
					GLint length;
					glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
					vector<char> err(length);
					glGetShaderInfoLog(shader, length, nullptr, &err[0]);

					cout << "Shader " << name << " compilation log:\n" << &err[0] << endl;
					//__android_log_write(ANDROID_LOG_ERROR, "shader", &err[0]);
				}
			}
			loaded_shaders.insert(std::pair<const char*, GLuint>(name, shader));
		}
		else
		{
			shader = loaded_shaders[name];
		}

		glAttachShader(prog, shader);
		++iter;

	}
	// We expect every shader to have a "position" attribute, to be the reference attribute
	glBindAttribLocation(prog, 0, "position");
	glLinkProgram(prog);
	{
		GLint status;
		glGetProgramiv(prog, GL_LINK_STATUS, &status);

		if(status) {
			GLint length;
			glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &length);
			vector<char> err(length);
			glGetProgramInfoLog(prog, length, nullptr, &err[0]);
			cout << "Linkage log of [" << *sources;
			for(iter = sources + 1; *iter; ++iter)
				cout << ", " << *iter;
			cout << "]:\n" << &err[0] << '\n';
			//__android_log_write(ANDROID_LOG_ERROR, "shader", &err[0]);
		}
	}

	transform = getUniform("transform");
	attr_texcoord = glGetAttribLocation(prog, "texcoord");
}

void CamShader::setup_shader(const char *sources[])
{
	Shader::setup_shader(sources);
	projection = getUniform("projection");
}

void CamShader::setProjection(const Matrix4& proj) const
{
	projection.set(proj);
}
