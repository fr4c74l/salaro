#pragma once

#include <vector>
#include <string>
#include <memory>
#include "gl.hpp"
#include "imgbuffer.hpp"
#include "puzzle.hpp"

void list_video_modes(std::vector<std::string> &out);
void post_redraw();

// File loading
std::unique_ptr<ImgBuffer> new_image_from_file(const std::string& filename);
std::vector<char> read_file(const std::string& filename);

// Input
void input_set_puzzle(Puzzle* puzzle);